with Ada.Text_IO;         use Ada.Text_IO;
with Ada.Integer_Text_IO; use Ada.Integer_Text_IO;
with affichage; use affichage;

package body resolutions is

   -----------------------
   -- estChiffreValable --
   -----------------------

   function estChiffreValable
     (g : in Type_Grille; v : Integer; c : Type_Coordonnee) return Boolean
   is
      ligne   : Type_Ensemble;
      colonne : Type_Ensemble;
      carre   : Type_Ensemble;
   begin
      if not caseVide (g, c) then
         raise CASE_NON_VIDE;
      end if;
      ligne   := obtenirChiffresDUneLigne (g, obtenirLigne (c));
      colonne := obtenirChiffresDUneColonne (g, obtenirColonne (c));
      carre   := obtenirChiffresDUnCarre (g, obtenirCarre (c));
      return
        not appartientChiffre (ligne, v) and
        not appartientChiffre (colonne, v) and
        not appartientChiffre (carre, v);
   end estChiffreValable;

   -------------------------------
   -- obtenirSolutionsPossibles --
   -------------------------------

   function obtenirSolutionsPossibles
     (g : in Type_Grille; c : in Type_Coordonnee) return Type_Ensemble
   is
      solpos : Type_Ensemble;
   begin
      if not caseVide (g, c) then
         raise CASE_NON_VIDE;
      end if;
      solpos := construireEnsemble;
      for i in Integer range 1 .. 9 loop
         if estChiffreValable (g, i, c) and not appartientChiffre (solpos, i)
         then
            ajouterChiffre (solpos, i);
         end if;
      end loop;
      return solpos;
   end obtenirSolutionsPossibles;

   ------------------------------------------
   -- rechercherSolutionUniqueDansEnsemble --
   ------------------------------------------

   function rechercherSolutionUniqueDansEnsemble
     (resultats : in Type_Ensemble) return Integer
   is
      val : Integer;
   begin
      if ensembleVide (resultats) then
         raise ENSEMBLE_VIDE;
      end if;

      if nombreChiffres (resultats) > 1 then
         raise PLUS_DE_UN_CHIFFRE;
      end if;

      for i in Integer range 1 .. 9 loop
         if appartientChiffre (resultats, i) then
            val := i;
         end if;
      end loop;

      return val;
   end rechercherSolutionUniqueDansEnsemble;

   --------------------
   -- resoudreSudoku --
   --------------------

   procedure resoudreSudoku (g : in out Type_Grille; trouve : out Boolean) is
      coor                : Type_Coordonnee;
      solutions_possibles : Type_Ensemble;
   begin
      solutions_possibles := construireEnsemble;
      while not estRemplie (g) loop
         for li in Integer range 1 .. 9 loop
            for col in Integer range 1 .. 9 loop
               coor := construireCoordonnees (li, col);
               if caseVide (g, coor) then
                  solutions_possibles := obtenirSolutionsPossibles (g, coor);
                  if nombreChiffres (solutions_possibles) = 1 then
                     fixerChiffre(g, coor, rechercherSolutionUniqueDansEnsemble(solutions_possibles));
                  end if;
               end if;
            end loop;
         end loop;
      end loop;
      trouve := True;
   end resoudreSudoku;

end resolutions;
