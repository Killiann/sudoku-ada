with Ada.Text_IO;         use Ada.Text_IO;
with Ada.Integer_Text_IO; use Ada.Integer_Text_IO;

package body ensemble is

   ------------------------
   -- construireEnsemble --
   ------------------------

   function construireEnsemble return Type_Ensemble is
      Ens : Type_Ensemble;
   begin
      for i in Integer range 1 .. 9 loop
         Ens (i) := False;
      end loop;

      return Ens;
   end construireEnsemble;

   ------------------
   -- ensembleVide --
   ------------------

   function ensembleVide (e : in Type_Ensemble) return Boolean is
      find : Boolean;
   begin
      find := False;
      for i in e'Range loop
         if e (i) then
            find := True;
            exit;
         end if;
      end loop;

      return not find;
   end ensembleVide;

   -----------------------
   -- appartientChiffre --
   -----------------------

   function appartientChiffre
     (e : in Type_Ensemble; v : Integer) return Boolean
   is
   begin
      return e (v);
   end appartientChiffre;

   --------------------
   -- nombreChiffres --
   --------------------

   function nombreChiffres (e : in Type_Ensemble) return Integer is
      compt : Integer;
   begin
      compt := 0;
      for i in e'Range loop
         if e (i) then
            compt := compt + 1;
         end if;
      end loop;

      return compt;
   end nombreChiffres;

   --------------------
   -- ajouterChiffre --
   --------------------

   procedure ajouterChiffre (e : in out Type_Ensemble; v : in Integer) is
   begin
      if e (v) then
         raise APPARTIENT_ENSEMBLE;
      end if;

      e (v) := True;
   end ajouterChiffre;

   --------------------
   -- retirerChiffre --
   --------------------

   procedure retirerChiffre (e : in out Type_Ensemble; v : in Integer) is
   begin
      if not e (v) then
         raise NON_APPARTIENT_ENSEMBLE;
      end if;

      e (v) := False;
   end retirerChiffre;

end ensemble;
