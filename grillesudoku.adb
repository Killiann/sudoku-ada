with Ada.Text_IO; use Ada.Text_IO;
with ada.Integer_Text_IO; use ada.Integer_Text_IO;
with affichage;   use affichage;

package body grilleSudoku is

   -- Tableau pour stocker des informations pour obtenirChiffresDUnCarre
   type iA is array (1 .. 2) of Integer;

   ----------------------
   -- construireGrille --
   ----------------------

   function construireGrille return Type_Grille is
      g : Type_Grille; --grille
   begin
      for L in Integer range 1 .. 9 loop
         for C in Integer range 1 .. 9 loop
            g (L, C) := 0;
         end loop;
      end loop;
      return g;
   end construireGrille;

   --------------
   -- caseVide --
   --------------

   function caseVide
     (g : in Type_Grille; c : in Type_Coordonnee) return Boolean
   is
   begin
      return g (obtenirLigne (c), obtenirColonne (c)) = 0;
   end caseVide;

   --------------------
   -- obtenirChiffre --
   --------------------

   function obtenirChiffre
     (g : in Type_Grille; c : in Type_Coordonnee) return Integer
   is
   begin
      if caseVide (g, c) then
         raise OBTENIR_CHIFFRE_NUL;
      end if;
      return g (obtenirLigne (c), obtenirColonne (c));
   end obtenirChiffre;

   --------------------
   -- nombreChiffres --
   --------------------

   function nombreChiffres (g : in Type_Grille) return Integer is
      nb : Integer;
   begin
      nb := 0;
      for L in Integer range 1 .. 9 loop
         for C in Integer range 1 .. 9 loop
            if g (L, C) /= 0 then
               nb := nb + 1;
            end if;
         end loop;
      end loop;

      return nb;
   end nombreChiffres;

   ------------------
   -- fixerChiffre --
   ------------------

   procedure fixerChiffre
     (g : in out Type_Grille; c : in Type_Coordonnee; v : in Integer)
   is
   begin
      if not caseVide (g, c) then
         raise FIXER_CHIFFRE_NON_NUL;
      end if;
      g (obtenirLigne (c), obtenirColonne (c)) := v;
   end fixerChiffre;

   ---------------
   -- viderCase --
   ---------------

   procedure viderCase (g : in out Type_Grille; c : in out Type_Coordonnee) is
   begin
      if caseVide (g, c) then
         raise VIDER_CASE_VIDE;
      end if;

      g (obtenirLigne (c), obtenirColonne (c)) := 0;
   end viderCase;

   ----------------
   -- estRemplie --
   ----------------

   function estRemplie (g : in Type_Grille) return Boolean is
      nb : Integer;
   begin
      nb := 0;
      for L in Integer range 1 .. 9 loop
         for C in Integer range 1 .. 9 loop
            if caseVide (g, construireCoordonnees (L, C)) then
               nb := nb + 1;
            end if;
         end loop;
      end loop;

      return nb = 0;
   end estRemplie;

   ------------------------------
   -- obtenirChiffresDUneLigne --
   ------------------------------

   function obtenirChiffresDUneLigne
     (g : in Type_Grille; numLigne : in Integer) return Type_Ensemble
   is
      e : Type_Ensemble;
   begin
      e := construireEnsemble;
      for i in 1 .. 9 loop
         if not caseVide (g, construireCoordonnees (numLigne, i)) then
            ajouterChiffre (e, (g (numLigne, i)));
         end if;
      end loop;

      return e;
   end obtenirChiffresDUneLigne;

   --------------------------------
   -- obtenirChiffresDUneColonne --
   --------------------------------

   function obtenirChiffresDUneColonne
     (g : in Type_Grille; numColonne : in Integer) return Type_Ensemble
   is
      e : Type_Ensemble;
   begin
      e := construireEnsemble;
      for i in 1 .. 9 loop
         if not caseVide (g, construireCoordonnees (i, numColonne)) then
            ajouterChiffre (e, (g (i, numColonne)));
         end if;
      end loop;

      return e;
   end obtenirChiffresDUneColonne;

   function getValueColonne (num : in Integer) return iA is
      a : iA;
   begin
      if num <= 3 then
         a (1) := 1;
         a (2) := 3;
      elsif num <= 6 then
         a (1) := 4;
         a (2) := 6;
      else
         a (1) := 7;
         a (2) := 9;
      end if;

      return a;
   end getValueColonne;

   function getValueLigne (num : in Integer) return iA is
      a : iA;
   begin
      if num = 1 or num = 4 or num = 7 then
         a (1) := 1;
         a (2) := 3;
      elsif num = 2 or num = 5 or num = 8 then
         a (1) := 4;
         a (2) := 6;
      else
         a (1) := 7;
         a (2) := 9;
      end if;

      return a;
   end getValueLigne;

   -----------------------------
   -- obtenirChiffresDUnCarre --
   -----------------------------

   function obtenirChiffresDUnCarre
     (g : in Type_Grille; numCarre : in Integer) return Type_Ensemble
   is
      e    : Type_Ensemble;
      c    : Type_Coordonnee;
      val1 : iA;
      val2 : iA;
   begin
      e := construireEnsemble;

      val1 := getValueLigne (numCarre);
      val2 := getValueColonne (numCarre);
      for i in Integer range val2 (1) .. val2 (2) loop
         for j in Integer range val1 (1) .. val1 (2) loop
            c := construireCoordonnees (i, j);
            if not caseVide (g, c) then
               ajouterChiffre (e, g (i, j));
            end if;
         end loop;
      end loop;
      return e;
   end obtenirChiffresDUnCarre;

end grilleSudoku;
